# DLFOV

A simple mod which sets the survivor's `CameraDefaultFOV` separate from the hunter. With minimal overhead thanks to specialized design for external loading.
